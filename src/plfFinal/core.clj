(ns examen.core)

(defn regresar-al-punto-de-origen?[entrada] 
  (cond 
    (= entrada "") true
    (= entrada []) true
    (= entrada (list)) true
    (= entrada nil) true
    :else 
    (let [
          f1 (fn[x] (= 0 (- (count (re-seq #"<" x)) 
                            (count (re-seq #">" x)))))
          f2 (fn[x] (= 0 (- (count (re-seq #"\^" x)) 
                            (count (re-seq #"v" x)))))
          ] 
         (cond
          (and (= true (f1 (apply str entrada))) (= true (f2 (apply str entrada)))) true
              (and (= false (f1 (apply str entrada))) (= false (f2 (apply str entrada)))) false
                 :else false))))
  (defn mismo-punto-final?[entrada entrada2] 
  (cond 
    (= entrada "") true
    (= entrada []) true
    (= entrada (list)) true
    (= entrada nil) true
    :else 
    (let [
          f1 (fn[x] (= 0 (- (count (re-seq #"<" x)) 
                            (count (re-seq #">" x)))))
          f2 (fn[x] (= 0 (- (count (re-seq #"\^" x)) 
                            (count (re-seq #"v" x)))))
          ] 
         (cond
          (and (= true (f1 (apply str entrada))) (= true (f2 (apply str entrada)))) true
              (and (= false (f1 (apply str entrada))) (= false (f2 (apply str entrada)))) false
                 :else false))))
 (defn coincidencias[entrada entrada2] 
  (cond 
    (= entrada "") true
    (= entrada []) true
    (= entrada (list)) true
    (= entrada nil) true
    :else 
    (let [
          f1 (fn[x] (= 0 (- (count (re-seq #"<" x)) 
                            (count (re-seq #">" x)))))
          f2 (fn[x] (= 0 (- (count (re-seq #"\^" x)) 
                            (count (re-seq #"v" x)))))
          ] 
         (cond
          (and (= true (f1 (apply str entrada))) (= true (f2 (apply str entrada)))) 1
              (and (= false (f1 (apply str entrada))) (= false (f2 (apply str entrada)))) 0
                 :else false))))
